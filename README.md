# visma/functions

Provides "missing PHP functions" that we frequently use.

## Installation

Install using [composer]

```bash
composer require visma/functions
```

## Functions
The following functions are provided by this library

### all_in_array
Check if all the provided values exist in an array

#### Description

```php
all_in_array ( array $needles , array $haystack [, bool $strict = FALSE ] ) : bool
```

Searches for **needles** in **haystack** using loose comparison unless **strict** is set.

### array_is_assoc

#### Description

```php
array_is_assoc ( array $array ) : bool
```

Checks if **array** has sequential integer keys (and therefore is a classic-non-associative array).

### iterable_to_array

#### Description

```php
iterable_to_array ( iterable $i [, bool $use_keys = TRUE ] ) : array
```

Copy the elements of an iterable **i** into an array.

### mb_array_change_key_case

#### Description

```php
mb_array_change_key_case (array $array [, int $mode = MB_CASE_LOWER [, ?string $enc = NULL ]]) : array
```

Returns an array with all keys from **array** case folded, converted in the way specified by **mode**. Numbered indices are left as is.

#### Parameters

##### mode

The *mode* parameter values are the same as in [mb_convert_case].

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### Tests
Run the test suite using composer:

```bash
composer test
```

The following tests must pass:

- [PHPUnit]
- [Psalm]
- [PHP_CodeSniffer]

## License
[MIT](https://choosealicense.com/licenses/mit/)

[composer]: https://getcomposer.org/
[PHP_CodeSniffer]: https://github.com/squizlabs/PHP_CodeSniffer
[PHPUnit]: https://phpunit.de/
[Psalm]: https://psalm.dev/
[mb_convert_case]: https://www.php.net/manual/en/function.mb-convert-case
