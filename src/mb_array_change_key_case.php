<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   MIT
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

if (!function_exists("mb_array_change_key_case")) {
    /**
     * Returns an array with all keys from array case folded, converted in the
     * way specified by mode, leaving numbered indices as is.
     *
     * @template V
     *
     * @param array       $array
     * @psalm-param  array<array-key, V> $array
     * @param int         $mode
     * @param string|null $enc
     * @return array
     * @psalm-return array<array-key, V>
     */
    function mb_array_change_key_case(array $array, int $mode = MB_CASE_LOWER, ?string $enc = null): array
    {
        $out = [];
        foreach ($array as $k => $v) {
            if (is_string($k)) {
                if ($enc !== null) {
                    $k = mb_convert_case($k, $mode, $enc);
                } else {
                    $k = mb_convert_case($k, $mode);
                }
            }
            $out[$k] = $v;
        }
        return $out;
    }
}
