<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   MIT
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

if (!function_exists("array_is_assoc")) {
    /**
     * Checks if an array has sequential integer keys (and therefore is a
     * classic-non-associative array)
     *
     * @param array $a
     * @return bool
     */
    function array_is_assoc(array $a): bool
    {
        if ($a === []) {
            return false;
        }

        $k = array_keys($a);
        $l = count($a);

        return range(0, $l - 1) !== $k;
    }
}
