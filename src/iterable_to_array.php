<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   MIT
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

if (!function_exists("iterable_to_array")) {
    /**
     * Copy the elements of an iterable **i** into an array.
     *
     * @template       T
     * @param          iterable    $i
     * @param          bool        $use_keys
     * @return         array
     * @see            https://www.php.net/manual/en/function.iterator-to-array.php
     * @psalm-param    iterable<T> $i
     * @psalm-return   array<T>
     */
    function iterable_to_array(iterable $i, bool $use_keys = true)
    {
        if (is_array($i)) {
            if ($use_keys) {
                return $i;
            } else {
                return array_values($i);
            }
        }

        /** @psalm-suppress RedundantConditionGivenDocblockType For future BC */
        if ($i instanceof Traversable) {
            return iterator_to_array($i, $use_keys);
        }

        throw new LogicException(
            "Encountered an iterable that was not an array and did not implement \Traversable"
        );
    }
}
