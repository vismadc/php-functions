<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   MIT
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

if (!function_exists("all_in_array")) {

    /**
     * Searches for needles in haystack, using loose comparison unless strict is set
     *
     * @param array<mixed> $needles
     * @param array<mixed> $haystack
     * @param bool  $strict
     * @return bool
     */
    function all_in_array(array $needles, array $haystack, bool $strict = false): bool
    {
        /** @psalm-var mixed $needle */
        foreach ($needles as $needle) {
            if (!in_array($needle, $haystack, $strict)) {
                return false;
            }
        }

        return true;
    }
}
