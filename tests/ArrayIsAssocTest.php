<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\Functions;

use PHPUnit\Framework\TestCase;

/**
 * @covers ::array_is_assoc()
 */
class ArrayIsAssocTest extends TestCase
{
    public function dataForTests(): array
    {
        return [
            [[], false],
            [["a", "b", "c"], false],
            [[1 => "a", 2 => "b", 3 => "c"], true],
            [["0" => "a", 1 => "b", "2" => "c"], false], // php casts int "string" keys to int
        ];
    }

    /**
     * @param array $array
     * @param bool  $expected
     * @dataProvider dataForTests
     */
    public function testAllInArray(array $array, bool $expected): void
    {
        $this->assertEquals(array_is_assoc($array), $expected);
    }
}
