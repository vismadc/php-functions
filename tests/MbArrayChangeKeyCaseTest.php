<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   MIT
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\Functions;

use PHPUnit\Framework\TestCase;

/**
 * @covers ::mb_array_change_key_case
 */
class MbArrayChangeKeyCaseTest extends TestCase
{
    public function dataForTests(): array
    {
        return [
            [["a", "A"], MB_CASE_LOWER, null, ["a"]],
            [["Æ", "Ø", "^"], MB_CASE_LOWER, null, ["æ", "ø", "^"]],
            [["Æ 🙂", "Ø", "^"], MB_CASE_LOWER, null, ["æ 🙂", "ø", "^"]],
        ];
    }

    /**
     * @param array $needles
     * @param array $haystack
     * @param bool  $strict
     * @param bool  $result
     * @dataProvider dataForTests
     */
    public function testAllInArray(array $keys, int $c, ?string $enc, array $expKeys): void
    {
        $a = array_combine($keys, range(0, (count($keys) - 1)));
        $this->assertEquals(array_keys(mb_array_change_key_case($a, $c, $enc)), $expKeys);
    }
}
