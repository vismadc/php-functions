<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   MIT
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\Functions;

use PHPUnit\Framework\TestCase;

/**
 * @covers ::iterable_to_array
 */
class IterableToArrayTest extends TestCase
{
    /**
     * @param iterable $i
     * @param bool     $use_keys
     * @param array    $a
     * @dataProvider expectItPasses
     */
    public function testCanConvertToArray(iterable $i, bool $use_keys, array $a): void
    {
        $this->assertEquals(iterable_to_array($i, $use_keys), $a);
    }

    public function expectItPasses(): array
    {
        return [
            [["a", "d" => "b", "c"], true, ["a", "d" => "b", "c"]],
            [new \ArrayIterator(["a", "b", "c"]), true, ["a", "b", "c"]],
            [$this->getGenerator(), false, ["b", "c", "f"]],
        ];
    }

    public function getGenerator(): \Generator
    {
        yield "a" => "b";
        yield "a" => "c";
        yield "d" => "f";
    }
}
