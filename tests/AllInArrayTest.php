<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   MIT
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\Functions;

use PHPUnit\Framework\TestCase;

/**
 * @covers ::all_in_array
 */
class AllInArrayTest extends TestCase
{
    public function dataForTests(): array
    {
        return [
            [["a"], ["a", "b"], true, true],
            [["a", "a"], ["a", "b"], true, true],
            [["a", "c"], ["a", "b"], true, false],
            [[null, 0, "0"], [0], false, true],
            [[null, 0, "0"], [0], true, false],
        ];
    }

    /**
     * @param array $needles
     * @param array $haystack
     * @param bool  $strict
     * @param bool  $result
     * @dataProvider dataForTests
     */
    public function testAllInArray(array $needles, array $haystack, bool $strict, bool $result): void
    {
        $this->assertEquals(all_in_array($needles, $haystack, $strict), $result);
    }
}
