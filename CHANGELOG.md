# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1]
### Added
- PHP 8 support

## [1.0.0]
### Added
- This changelog
- The README file
- `all_in_array`
- `array_is_assoc`
- `iterable_to_array`
- `mb_array_change_key_case`

[Unreleased]: https://bitbucket.org/vismadc/php-functions/src/master/
[1.0.0]: https://bitbucket.org/vismadc/php-functions/src/1.0.0/
[1.0.1]: https://bitbucket.org/vismadc/php-functions/src/1.0.1/
